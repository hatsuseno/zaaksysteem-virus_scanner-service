FROM registry.gitlab.com/zaaksysteem/zaaksysteem-service-http-library:d190b61a

ENV SERVICE_HOME /opt/zaaksysteem-virus_scanner-service

WORKDIR $SERVICE_HOME

ENV ZSVC_LOG4PERL_CONFIG=/etc/zaaksysteem-virus_scanner-service/log4perl.conf \
    ZSVC_SERVICE_CONFIG=/etc/zaaksysteem-virus_scanner-service/zaaksysteem-virus_scanner-service.conf

COPY Makefile.PL ./
RUN cpanm --installdeps . && rm -rf ~/.cpanm || (cat ~/.cpanm/work/*/build.log && false)

COPY bin ./bin
COPY xt  ./xt
COPY t   ./t
COPY etc  /etc/zaaksysteem-virus_scanner-service

COPY lib ./lib

CMD starman $SERVICE_HOME/bin/zaaksysteem-virus_scanner-service.psgi

EXPOSE 5000
